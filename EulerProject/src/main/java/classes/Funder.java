package classes;

import java.util.List;
import java.util.Objects;

public class Funder extends Counterpart<Object> {

	String name;
	List<String> adresses;
	
	public Funder() {
		super();
	}

	public String getName() {
		return ("Funder's name : " + name);
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void setAdresses(List<String> adresses) {
		this.adresses = adresses;
	}

	public List<String> getAdresses() {
		return adresses;
	}
	
	/*
	 * M�thode sp�cifique � la classe Funder.
	 * Un objet Counterpart ne peut pas l'utiliser.
	 */
	public String concatWords(String word1, String word2) {
		return word1.concat(word2);
	}

	@Override
	public int hashCode() {
		return Objects.hash(adresses, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Funder))
			return false;
		Funder other = (Funder) obj;
		return Objects.equals(adresses, other.adresses) && Objects.equals(name, other.name);
	}	
	
	
}
