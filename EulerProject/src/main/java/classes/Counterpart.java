package classes;

import java.util.List;

public abstract class Counterpart<T> {

	protected static Supplier SUPPLIER_SINGLETON;
	protected static Funder FUNDER_SINGLETON;
	protected java.util.function.Supplier<T> counterpartSupplier;

	protected Counterpart() {
	}

	private static java.util.function.Supplier<Supplier> supp = () -> {
		return SUPPLIER_SINGLETON == null ? getInstanceOf(TYPE.SUPPLIER.ofClass()) : SUPPLIER_SINGLETON;
	};
	private static java.util.function.Supplier<Funder> fund = () -> {
		return FUNDER_SINGLETON == null ? getInstanceOf(TYPE.FUNDER.ofClass()) : FUNDER_SINGLETON;
	};

	public static Counterpart<?> getSupplier() {
		return supp.get();
	}

	public static Counterpart<?> getFunder() {
		return fund.get();
	}

	public static Counterpart<?> getSupplierWithName(String name) {
		Supplier s = supp.get();
		s.setName(name);
		return s;
	}

	public static Counterpart<?> getFunderWithName(String name) {
		Funder f = fund.get();
		f.setName(name);
		return f;
	}

	public static String funderWordsConcatenate(String word1, String word2) {
		Funder f = fund.get();
		return f.concatWords(word1, word2);
	}

	public static Supplier getInstanceOfSupplier() {
		return supp.get();
	}

	public static Funder getInstanceOfFunder() {
		return fund.get();
	}

	// ------------------- ABSTRACT ----------------------------

	public abstract void setName(String name);

	public abstract String getName();

	public abstract void setAdresses(List<String> adresses);

	public abstract List<String> getAdresses();

	// ----------------- OWN PUBLIC METHODS --------------------------

	/*
	 * Cette m�thode concr�te de la classe m�re est accessible par toutes les
	 * classes filles.
	 */
	public void printWord(String word) {
		System.out.printf("Mot %s re�u en argument !", word);
	}

	// ----------------- OWN PRIVATE METHODS --------------------------

	@SuppressWarnings("unchecked")
	private static <T> T getInstanceOf(Class<?> c) {
		if (!(c.getSuperclass() == Counterpart.class)) {
			System.out.printf("%s type not allowed !", c.getCanonicalName());
		} else {
			if (c.equals(Supplier.class)) {
				if (SUPPLIER_SINGLETON == null) {
					SUPPLIER_SINGLETON = new Supplier();
					System.out.println("Creating instance of Supplier -> " + SUPPLIER_SINGLETON);
				}
				System.out.println("Returning instance #" + SUPPLIER_SINGLETON);
				return (T) SUPPLIER_SINGLETON;
			} else if (c.equals(Funder.class)) {
				if (FUNDER_SINGLETON == null) {
					FUNDER_SINGLETON = new Funder();
					System.out.println("Creating instance of Funder -> " + FUNDER_SINGLETON);
				}
				System.out.println("Returning instance #" + FUNDER_SINGLETON);
				return (T) FUNDER_SINGLETON;
			}
		}
		return null;
	}

	private static enum TYPE {
		SUPPLIER(Type.SUPPLIER), FUNDER(Type.FUNDER);

		private Class<?> clazz;

		TYPE(Class<?> c) {
			this.clazz = c;
		}

		private static class Type {
			static final Class<?> SUPPLIER = Supplier.class;
			static final Class<?> FUNDER = Funder.class;
		}

		public Class<?> ofClass() {
			return clazz;
		}

	}

}
