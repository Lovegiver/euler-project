package classes;

import java.util.List;
import java.util.Objects;

public class Supplier extends Counterpart<Object> {

	String name;
	List<String> adresses;
	
	public Supplier() {
		super();
	}

	public String getName() {
		return ("Supplier's name : " + name);
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void setAdresses(List<String> adresses) {
		this.adresses = adresses;
	}
	
	public List<String> getAdresses() {
		return adresses;
	}

	@Override
	public int hashCode() {
		return Objects.hash(adresses, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Supplier))
			return false;
		Supplier other = (Supplier) obj;
		return Objects.equals(adresses, other.adresses) && Objects.equals(name, other.name);
	}
	
}
