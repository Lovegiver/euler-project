/**
 * 
 */
package classes;

import java.util.function.UnaryOperator;
import java.util.stream.LongStream;
import java.util.stream.Stream;

/**
 * @author Loveg
 *
 */
public class Utilz {

	public static boolean isPrime(final long value) {
		boolean isPrime = false;
		long limit = (long) (value > 1L ? Math.sqrt(value) : 0);
		if(!(limit == 0)) {
			isPrime = LongStream.rangeClosed(2, limit)
					.noneMatch(divisor -> value % divisor == 0);
		}
		return isPrime;
	}
	
	public static LongStream getLongStream(final long root, final long limit) {
		LongStream ls = null;
		if(limit >= root) {
			ls = LongStream.rangeClosed(root, limit);
		}
		return ls;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> Stream<T> getStream(final long root, final long limit, final int step) {
		Stream<T> s = null;
		UnaryOperator<Long> stepOn = i -> i + step;
		if(limit >= root) {
			s = (Stream<T>) Stream.iterate(root, stepOn).limit(limit);
		}
		return s;
	}
	
}
