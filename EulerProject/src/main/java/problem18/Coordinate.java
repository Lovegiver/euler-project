/**
 * 
 */
package problem18;

import java.util.Objects;

/**
 * @author Fred
 *
 */
public class Coordinate {

	private int abscissX;
	private int ordinateY;
	
	public Coordinate (int x, int y) {
		this.abscissX = x;
		this.ordinateY = y;
	}

	public int getAbscissX() {
		return abscissX;
	}

	public void setAbscissX(int abscissX) {
		this.abscissX = abscissX;
	}

	public int getOrdinateY() {
		return ordinateY;
	}

	public void setOrdinateY(int ordinateY) {
		this.ordinateY = ordinateY;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(abscissX, ordinateY);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Coordinate))
			return false;
		Coordinate other = (Coordinate) obj;
		return abscissX == other.abscissX && ordinateY == other.ordinateY;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Coordinate [abscissX=");
		builder.append(abscissX);
		builder.append(", ordinateY=");
		builder.append(ordinateY);
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
