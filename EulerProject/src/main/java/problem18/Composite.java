/**
 * 
 */
package problem18;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * @author Fred
 *
 */
public interface Composite extends Comparable<Composite> {

	/**
	 * Return the INTEGER value of current {@link Composite}.<br>
	 * 
	 * @return
	 */
	Integer getValue();

	/**
	 * Return the parents of the current {@link Composite}.<br>
	 * Cell located at the top of the triangle will return an empty list.<br>
	 * Cells located on the border will return a list made of a single parent.<br>
	 * Other cells should return a 2 parents list.<br>
	 * 
	 * @return a list of 0, 1 or 2 cells implementing {@link Composite} interface.
	 */
	List<Composite> getParents();

	/**
	 * Return the childs of the current {@link Composite}.<br>
	 * Cell located at the bottom of the triangle will return an empty list.<br>
	 * Cells located on the border will return a list made of a single child.<br>
	 * Other cells should return a 2 childs list.<br>
	 * 
	 * @return a list of 0, 1 or 2 cells implementing {@link Composite} interface.
	 */
	List<Composite> getChilds();

	/**
	 * Return the highest-valued child from a cell's list of childs.<br>
	 * 
	 * @return Child with highest value, NULL if no childs are available
	 */
	default Optional<Composite> getGreatestChild() {
		Optional<Composite> child;
		child = getChilds().stream().max((x, y) -> x.compareTo(y));
		return child.isPresent() ? child : null;
	}

	default Optional<Composite> getGreatestParent() {
		Optional<Composite> parent;
		parent = getParents().stream().max((x, y) -> x.compareTo(y));
		return parent.isPresent() ? parent : null;
	}

	/**
	 * Recursive method building a path of {@link Node} from top to bottom of the {@link Grid}.<br>
	 * Each analyzed {@link Node} calls this method in a recursive way for each of its child {@link Node}s 
	 * until analyze depth reaches integer '0' value, letting user decide depth analysis.<br>
	 * 
	 * @param node : the {@link Node} to add to current path
	 * @param analyzeDepth : integer value decremented for each Node added to current path
	 * @param currentPath : list of {@link Node} objects linked together by a parent/child relationship and making a path through the {@link Grid}
	 * @param paths : a list of all completed paths
	 * @return the paths list
	 */
	default List<List<Composite>> buildAllPaths(Composite node, int analyzeDepth, List<Composite> currentPath,
			List<List<Composite>> paths) {
		currentPath.add(node);
		List<Composite> alternatePath = null;
		if (analyzeDepth > 0) {
			List<Composite> childs = node.getChilds();
			for (Composite child : childs) {
				alternatePath = new LinkedList<Composite>(currentPath);
				buildAllPaths(child, analyzeDepth - 1, alternatePath, paths);
			}
		} else {
			paths.add(currentPath);
		}
		return paths;
	}

	/**
	 * Initialize paths analysis starting from a given {@link Node} and for a given 'depth'.<br>
	 * Analysis depth represents the number of lines being analyzed together.<br>
	 * If analysis depth is integer '0', only the given starting node will be analyzed and included in the path being built.<br>
	 * If analysis depth is integer '1', the given node and all its children will be analyzed.<br>
	 * If analysis depth is integer '2', all children of each children will be analyzed, etc.<br>
	 * This behavior is managed by {@link Composite#buildAllPaths(Composite, int, List, List)} method which calls itself in a recursive way.<br>
	 * 
	 * @param node : the {@link Node} to add to current path
	 * @param analyzeDepth : integer value decremented for each Node added to current path
	 * @return the path (list of nodes) with the highest sum value
	 */
	default List<Composite> buildMaxPath(Composite node, int analyzeDepth) {
		List<List<Composite>> paths = new LinkedList<List<Composite>>();
		List<Composite> maxPath = new LinkedList<Composite>();

		paths = node.buildAllPaths(node, analyzeDepth, maxPath, paths);
//		System.out.println("PATHS size = " + paths.size());
//		for (List<Composite> list : paths) {
//			System.out.println("Liste " + list.hashCode());
//			for (Composite c : list) {
//				System.out.println("\t--> Composite = " + c.getCoordinate() + " | " + c.getValue());
//			}
//		}
		int maxValue = 0;
		for (List<Composite> list : paths) {
			int value = list.stream().map(c -> c.getValue()).reduce(0, (x, y) -> x + y);
			if (value > maxValue) {
				maxValue = value;
				maxPath = list;
			}
		}
		System.out.println("Total value for MaxPath = " + maxValue);
		return maxPath;
	}

	/**
	 * For each {@link Node}, fill its 'childs' property.<br>
	 * This action is delegated to the {@link Grid} object but remains in the
	 * {@link Node} scope.<br>
	 * 
	 * @return the Node itself
	 */
	Node fillChilds();

	/**
	 * For each {@link Node}, returns its {@link Coordinate} property.<br>
	 * 
	 * @return a {@link Coordinate} object indicating X and Y location of current
	 *         node in the Grid.<br>
	 */
	Coordinate getCoordinate();

}
