/**
 * 
 */
package problem18;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Fred
 *
 */
public class Node implements Composite {

	/*
	 * Each Node has a reference on the Grid object containing it. This allows
	 * method delegation to the Grid object
	 */
	private Grid gridDelegate;
	
	private final int value;
	private final Coordinate coordinate;
	private List<Composite> parents;
	private List<Composite> childs;

	/**
	 * Private constructor for {@link Node} object.<br>
	 * The static {@link Node#getNode(int, int, int)} method is the real public
	 * interface.<br>
	 * 
	 * @param value
	 * @param coordinate
	 */
	private Node(int value, Coordinate coordinate) {
		super();
		this.value = value;
		this.coordinate = coordinate;
		parents = new ArrayList<Composite>();
		childs = new ArrayList<Composite>();
	}

	/**
	 * Public interface shown to user instead of constructor.<br>
	 * Insures given value and coordinates will not be null before calling
	 * constructor.<br>
	 * Coordinates values are considered from top to botom of the {@link Grid} with
	 * absciss X for columns and ordinate Y for lines.<br>
	 * The Node at the top of the Grid is located at (X,Y) -> (0,0). Its children
	 * are respectively located at (X,Y) -> (0,1) and (X,Y) -> (1,1). Etc.<br>
	 * 
	 * @param value    : the integer value of the Node
	 * @param absciss  : the X (column) location
	 * @param ordinate : the Y (line) location
	 * @return a Node object, like constructor would do
	 */
	public static Node getNode(int value, int absciss, int ordinate) {
		Objects.requireNonNull(value, "Null value unauthorized.");
		Objects.requireNonNull(absciss, "Null X unauthorized.");
		Objects.requireNonNull(ordinate, "Null Y unauthorized.");
		Coordinate coord = new Coordinate(absciss, ordinate);
		return new Node(value, coord);
	}

	/**
	 * Implementation of the {@link Comparable} interface, inherited from
	 * {@link Composite} interface.<br>
	 */
	@Override
	public int compareTo(Composite o) {
		if (o != null) {
			return Integer.compare(this.getValue(), o.getValue());
		} else {
			return 0;
		}
	}

	@Override
	public Integer getValue() {
		return this.value;
	}

	@Override
	public List<Composite> getParents() {
		return this.parents;
	}

	@Override
	public List<Composite> getChilds() {
		return this.childs;
	}

	public void setChilds(List<Composite> childs) {
		this.childs = childs;
	}

	public void setParents(List<Composite> parents) {
		this.parents = parents;
	}

	@Override
	public Coordinate getCoordinate() {
		return this.coordinate;
	}
	
	public int getX() {
		return this.coordinate.getAbscissX();
	}
	
	public int getY() {
		return this.coordinate.getOrdinateY();
	}

	/**
	 * @return the gridDelegate
	 */
	public Grid getGridDelegate() {
		return gridDelegate;
	}

	/**
	 * @param gridDelegate the gridDelegate to set
	 */
	public void setGridDelegate(Grid gridDelegate) {
		this.gridDelegate = gridDelegate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see problem18.Composite#fillChilds()
	 */
	@Override
	public Node fillChilds() {
		this.gridDelegate.fillChilds(this);
		return this;
	}
	
	public Node addParent(Composite cell) {
		this.parents.add(cell);
		return this;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(coordinate, value);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Node))
			return false;
		Node other = (Node) obj;
		return Objects.equals(coordinate, other.coordinate) && value == other.value;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Node [value=");
		builder.append(value);
		builder.append(", coordinate=");
		builder.append(coordinate);
		builder.append(", parents=");
		builder.append(parents);
		builder.append(", childs=");
		builder.append(childs);
		builder.append("]");
		return builder.toString();
	}
	
	

}
