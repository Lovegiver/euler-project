/**
 * 
 */
package problem18;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * The Grid object is a container of {@link Node} objects.<br>
 * It holds the 'structure' of {@link Node} organization.<br>
 * 
 * @author Fred
 *
 */
public class Grid {

	private static final int WIDTH = 15;
	private static final int HEIGHT = 15;

	private final List<Composite> cells;

	/**
	 * The constructor of the {@link Grid} object takes all the cells (or nodes) as
	 * argument.<br>
	 * For each of its node, it will fill the {@link Node#Grid} property to enable
	 * delegation.<br>
	 * 
	 * @param cells
	 */
	public Grid(List<Composite> cells) {
		this.cells = cells;
		cells.stream().forEach(cell -> ((Node) cell).setGridDelegate(this));
	}

	/**
	 * @return the cells
	 */
	public List<Composite> getCells() {
		return cells;
	}

	/**
	 * For a given {@link Node}, looks for all existing children in the
	 * {@link Grid}.<br>
	 * Note that each node owns a reference on the Grid object and also implements a
	 * {@link Composite#fillChilds()} method.<br>
	 * Each time a node invokes its own 'fillChilds()' method, Grid delegation calls
	 * the {@link Grid#fillChilds(Composite)} method with the caller node as
	 * argument.<br>
	 * 
	 * @param node : the {@link Node} being analyzed for children
	 * @return the Node itself
	 */
	public Node fillChilds(Composite node) {
		List<Composite> childs = new ArrayList<Composite>();

		/*
		 * For the given Node argument, checks the presence of children in the Grid. All
		 * the Grid will be parsed in order to find 2, 1 or 0 child.
		 */
		Predicate<? super Composite> isChild = c -> {
			Coordinate coord = ((Node) c).getCoordinate();
			int x = coord.getAbscissX();
			int y = coord.getOrdinateY();

			if ((y == ((Node) node).getCoordinate().getOrdinateY() + 1
					&& x == ((Node) node).getCoordinate().getAbscissX())
					|| (x == ((Node) node).getCoordinate().getAbscissX() + 1
							&& y == ((Node) node).getCoordinate().getOrdinateY() + 1)) {
				return Boolean.TRUE;
			} else {
				return Boolean.FALSE;
			}
		};

		/*
		 * Parses the whole Grid to find potential children, using the 'isChild'
		 * predicate. If one ore two children are found, the Node.Childs property will
		 * be set with them.
		 */
		childs = cells.stream()
				.filter(c -> ((Node) c).getCoordinate().getOrdinateY() >= ((Node) node).getCoordinate().getOrdinateY())
				.filter(isChild)
				.collect(Collectors.toList());
		((Node) node).setChilds(childs);
		return (Node) node;
	}

	@SuppressWarnings("unused")
	private boolean isInGrid(int x, int y) {
		return (x >= 0 && x < WIDTH && y >= 0 && y < HEIGHT);
	}
}
