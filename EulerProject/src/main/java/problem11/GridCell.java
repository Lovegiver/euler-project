package problem11;

public class GridCell implements Composite {

	private final int x;
	private final int y;
	private final int value;
	
	protected GridCell(int x, int y, int value) {
		this.x = x;
		this.y = y;
		this.value = value;
	}
	
	@Override
	public long getProduct() {
		return this.value;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getValue() {
		return value;
	}
	
	

}
