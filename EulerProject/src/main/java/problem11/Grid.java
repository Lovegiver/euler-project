package problem11;

import java.util.HashSet;
import java.util.Set;

import problem11.CellsShape.Type;

public class Grid implements Composite {

	private final static int LIMIT = 4;
	private final static int GRID_MIN_VERTICAL = 19;
	private final static int GRID_MAX_VERTICAL = 0;
	private final static int GRID_MIN_HORIZONTAL = 0;
	private final static int GRID_MAX_HORIZONTAL = 19;

	private static Grid INSTANCE = null;
	private static int[][] grid;
	private static GridCell[][] cellsGrid;
	private static Set<Composite> northEast;
	private static Set<Composite> east;
	private static Set<Composite> southEast;
	private static Set<Composite> south;

	private Grid(int[][] intGrid) {
		grid = intGrid;
		Grid.northEast = new HashSet<Composite>();
		Grid.east = new HashSet<Composite>();
		Grid.southEast = new HashSet<Composite>();
		Grid.south = new HashSet<Composite>();
	}

	/**
	 * Instead of calling the natural constructor, we call a static method
	 * returning a Singleton of {@link Grid} (just for pleasure).<br>
	 * 
	 * @param intGrid : the 2D int array built at the very start of the app
	 * @return a unique instance of Grid
	 */
	public static Grid INSTANCE(int[][] intGrid) {
		System.out.println("Creating Singleton Grid instance.");
		if (INSTANCE == null) {
			INSTANCE = new Grid(intGrid);
		}
		return INSTANCE;
	}

	/**
	 * Once the {@link Grid} has been instanciated, it can be analyzed and sliced 
	 * in multiple shapes of cells called {@link CellsShape}.<br>
	 * 
	 * @return the GRID instance
	 * @throws IllegalAccessException
	 */
	public Grid SLICE() throws IllegalAccessException {
		if (INSTANCE == null) {
			throw new IllegalAccessException("You should create Grid instance first !");
		} else {
			// GRID building
			initCellsObjects();
			// For each Cell of the GRID, we want to know if a shape is buildable
			// in NorthEast, East, SouthEast and South directions.
			for (int line = 0; line <= GRID_MAX_HORIZONTAL; line++) {
				for (int column = 0; column <= GRID_MIN_VERTICAL; column++) {
					// GRID is analyzed from Top-Left (X=0,Y=0) corner to Bottom-Right (X=19,Y=19) corner
					GridCell currentCell = cellsGrid[column][line];
					if (remainsInGrid(currentCell.getX() + (LIMIT - 1), currentCell.getY() - (LIMIT - 1))) {
						CellsShape shape = new CellsShape(Type.NORTH_EAST, currentCell);
						for (int i = 1; i < LIMIT; i++) {
							shape.addCell(cellsGrid[currentCell.getX() + i][currentCell.getY() - i]);
						}
						northEast.add(shape);
					}
					if (remainsInGrid(currentCell.getX() + (LIMIT - 1), currentCell.getY())) {
						CellsShape shape = new CellsShape(Type.EAST, currentCell);
						for (int i = 1; i < LIMIT; i++) {
							shape.addCell(cellsGrid[currentCell.getX() + i][currentCell.getY()]);
						}
						east.add(shape);
					}
					if (remainsInGrid(currentCell.getX() + (LIMIT - 1), currentCell.getY() + (LIMIT - 1))) {
						CellsShape shape = new CellsShape(Type.SOUTH_EAST, currentCell);
						for (int i = 1; i < LIMIT; i++) {
							shape.addCell(cellsGrid[currentCell.getX() + i][currentCell.getY() + i]);
						}
						southEast.add(shape);
					}
					if (remainsInGrid(currentCell.getX(), currentCell.getY() + (LIMIT - 1))) {
						CellsShape shape = new CellsShape(Type.SOUTH, currentCell);
						for (int i = 1; i < LIMIT; i++) {
							shape.addCell(cellsGrid[currentCell.getX()][currentCell.getY() + i]);
						}
						south.add(shape);
					}
				}
			}
		}
		return INSTANCE;
	}

	@Override
	public long getProduct() {
		return 0;
	}

	public Set<Composite> getNorthEast() {
		return northEast;
	}

	public void setNorthEast(Set<Composite> northEast) {
		Grid.northEast = northEast;
	}

	public Set<Composite> getEast() {
		return east;
	}

	public void setEast(Set<Composite> east) {
		Grid.east = east;
	}

	public Set<Composite> getSouthEast() {
		return southEast;
	}

	public void setSouthEast(Set<Composite> southEast) {
		Grid.southEast = southEast;
	}

	public Set<Composite> getSouth() {
		return south;
	}

	public void setSouth(Set<Composite> south) {
		Grid.south = south;
	}

	public int[][] getGrid() {
		return grid;
	}

	/**
	 * For given coordinates, we want to validate if they remain inside the GRID.<br>
	 * 
	 * @param targetX (column)
	 * @param targetY (line)
	 * @return
	 */
	private static boolean remainsInGrid(int targetX, int targetY) {
		boolean isInside = Boolean.FALSE;
		if (targetY >= GRID_MAX_VERTICAL 
				&& targetY <= GRID_MIN_VERTICAL 
				&& targetX >= GRID_MIN_HORIZONTAL
				&& targetX <= GRID_MAX_HORIZONTAL) {
			isInside = Boolean.TRUE;
		}
		return isInside;
	}

	/**
	 * The cellsGrid property is initialized with a 2D {@link GridCell} array.<br>
	 * For each value contained in the 2D int array, we build a corresponding GridCell object
	 * with usefull properties : (X,Y) coordinates and value.<br><br>
	 * Note : X are columns, Y are lines
	 * 
	 */
	private static void initCellsObjects() {
		cellsGrid = new GridCell[20][20];
		for (int line = 0; line <= GRID_MAX_HORIZONTAL; line++) {
			for (int column = 0; column <= GRID_MIN_VERTICAL; column++) {
				cellsGrid[column][line] = new GridCell(column, line, grid[column][line]);
			}
		}
	}

}
