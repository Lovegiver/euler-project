package problem11;

import java.util.HashSet;
import java.util.Set;

public class CellsShape implements Composite {

	private final int LIMIT = 4;
	private final int GRID_MIN_VERTICAL = 19;
	private final int GRID_MAX_VERTICAL = 0;
	private final int GRID_MAX_HORIZONTAL = 19;

	public enum Type {
		NORTH_EAST, EAST, SOUTH_EAST, SOUTH
	};

	private final Type type;
	private Set<Composite> cells;
	private final GridCell BASE_CELL;

	/**
	 * All {@link CellsShape} will be built starting from each
	 * individual {@link GridCell} of the GRID.<br>
	 * Note that all types won't be possible depending on the given cell's coorinates.<br>
	 * For example, it will not be possible to build a NorthEast CellsShape 
	 * starting from cell located at (X=0,Y=0) coordinates because it will
	 * end outside of the GRID limits.<br><br>
	 * 
	 * Each CellsShape is initialized with 2 arguments :<br>
	 * 
	 * @param type : NorthEast, East, SouthEast, South
	 * @param cell : origin cell (BASE CELL)
	 */
	protected CellsShape(Type type, Composite cell) {
		this.cells = new HashSet<Composite>(LIMIT);
		this.type = type;
		this.BASE_CELL = (GridCell) cell;
		addCell(BASE_CELL);
	}

	@Override
	public long getProduct() {
		long result = 1;
		for (Composite c : cells) {
			result *= c.getProduct();
		}
		return result;
	}

	/**
	 * Once the {@link CellsShape} has been initialized with a BASE_CELL as origin,
	 * the {@link Grid#SLICE()} method will try to populate all types of {@link CellsShape}.<br>
	 * The {@link CellsShape#addCell(GridCell)} method will add a given
	 * {@link GridCell} to the shape only if coordinates are valid.<br>
	 * 
	 * 
	 * @param c
	 * @return
	 */
	public CellsShape addCell(GridCell c) {
		if (cells.size() < LIMIT && isNewCellValid(c)) {
			cells.add(c);
		} else {
			if (cells.size() < LIMIT) {
				System.out.printf("\nSupplied GridCell (X = %d, Y = %d) is not compliant "
						+ "with %s direction starting from BASE CELL located at (X = %d, Y = %d)",
						c.getX(),c.getY(),type.name(),BASE_CELL.getX(),BASE_CELL.getY());
			} else {
				System.out.println("CellsShape full !");
			}
		}
		return this;
	}

	private boolean isNewCellValid(GridCell cell) {
		boolean isValid = Boolean.FALSE;
		switch (this.type) {
		case NORTH_EAST:
			isValid = isNorthEastCompliant(cell);
			break;
		case EAST:
			isValid = isEastCompliant(cell);
			break;
		case SOUTH:
			isValid = isSouthCompliant(cell);
			break;
		case SOUTH_EAST:
			isValid = isSouthEastCompliant(cell);
			break;
		default:
			break;
		}
		return isValid;
	}


	private boolean isNorthEastCompliant(GridCell cell) {
		boolean isValid = Boolean.FALSE;
		int maxHeight = BASE_CELL.getY() - (LIMIT - 1);
		int maxWidth = BASE_CELL.getX() + (LIMIT - 1);
		if (cells.size() == 0) {
			isValid =  Boolean.TRUE;
		} else {
			if (maxHeight >= GRID_MAX_VERTICAL && maxWidth <= GRID_MAX_HORIZONTAL) {
				isValid = Boolean.TRUE;
			}
		}
		return isValid;
	}
	
	private boolean isEastCompliant(GridCell cell) {
		boolean isValid = Boolean.FALSE;
		int maxWidth = BASE_CELL.getX() + (LIMIT - 1);
		if (cells.size() == 0) {
			isValid =  Boolean.TRUE;
		} else {
			if (maxWidth <= GRID_MAX_HORIZONTAL) {
				isValid = Boolean.TRUE;
			}
		}
		return isValid;
	}
	
	private boolean isSouthEastCompliant(GridCell cell) {
		boolean isValid = Boolean.FALSE;
		int maxHeight = BASE_CELL.getY() + (LIMIT - 1);
		int maxWidth = BASE_CELL.getX() + (LIMIT - 1);
		if (cells.size() == 0) {
			isValid =  Boolean.TRUE;
		} else {
			if (maxHeight <= GRID_MIN_VERTICAL && maxWidth <= GRID_MAX_HORIZONTAL) {
				isValid = Boolean.TRUE;
			}
		}
		return isValid;
	}
	
	private boolean isSouthCompliant(GridCell cell) {
		boolean isValid = Boolean.FALSE;
		int maxHeight = BASE_CELL.getY() + (LIMIT - 1);
		if (cells.size() == 0) {
			isValid =  Boolean.TRUE;
		} else {
			if (maxHeight >= GRID_MAX_VERTICAL) {
				isValid = Boolean.TRUE;
			}
		}
		return isValid;
	}

	public Set<Composite> getCells() {
		return cells;
	}

	public void setCells(Set<Composite> cells) {
		this.cells = cells;
	}

	public Type getType() {
		return type;
	}

	
	
}
