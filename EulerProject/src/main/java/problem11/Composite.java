package problem11;

public interface Composite {

	long getProduct();
	
}
