package problem17;

import org.apache.commons.lang3.StringUtils;

/**
 * This is the main class, doing most of the job.<br>
 * First it converts a value from INT to STRING : 23 -> 0023.<br>
 * Then it converts the STRING to WORD : 0023 -> TWENTYTHREE.<br>
 * <br>
 * @author Loveg
 *
 */
public class Converter {

	final private int value;
	
	final private static String EMPTY = "";
	final private static String ZERO = "0";
	final private static String AND = "AND";
	final private static int SIZE = 4;

	private Converter(int value) {
		this.value = value;
	}
	
	public static int lengthOf(int val) {
		return wordsOf(stringOf(val)).length();
	}

	/**
	 * Takes an INT value and transform it into a 4 characters STRING.<br>
	 * String is padded with ZEROs when needed.<br>
	 * @param value : the INT value to stringify
	 * @return a String representation of the given value
	 */
	public static String stringOf(int value) {
		Converter converter = new Converter(value);
		String valueToConvert = null;
		valueToConvert = Integer.toString(converter.value);
		valueToConvert = StringUtils.leftPad(valueToConvert, SIZE, ZERO);
		System.out.printf("\nConverter : stringOf(%d) -> %s", value, valueToConvert);
		return valueToConvert;
	}

	/**
	 * 
	 * @param value : a String representation of the given value
	 * @return a number written with WORDS instead of NUMBERS : <br>
	 * 4 -> FOUR<br>
	 * 218 -> TWO HUNDRED AND EIGHTEEN<br>
	 */
	public static String wordsOf(String value) {
		StringBuilder sb = new StringBuilder();
		/*
		 *  Numbers on the right are specific.
		 *  Values between [1 - 19] inclusive use particular words.
		 *  If present, those words have to be prefixed by the "AND" locution.
		 */
		boolean areRightNumbersProcessed = Boolean.FALSE;
		int rightNumbers = Integer.parseInt(value.substring(2));
		System.out.printf("\nConverter : wordsOf(%s)", value);
		/*
		 *  We loop over each character of the String in order to translate it
		 *  with WORDS.
		 *  Example : INT value 23 has been translated in a "0023" string.
		 *  We have to process each of these characters : 0 -> 0 -> 2 -> 3
		 */
		for (int position = 0; position < value.length(); position++) {
			char c = value.charAt(position);
			// If the 2 right numbers are contained in the 0-19 interval
			if (position > 1 && rightNumbers > 0 && rightNumbers < 20 && (!areRightNumbersProcessed)) {
				// If the 2 left numbers are not ZERO
				if (position > 1 && sb.length() > 0) {
					sb.append(AND);
				}
				sb.append(Unit.getConvertibleString(rightNumbers));
				areRightNumbersProcessed = Boolean.TRUE;
			} else if (!areRightNumbersProcessed) {
				if (position == 2 && sb.length() > 0 && rightNumbers != 0) {
					sb.append(AND);
				}
				sb.append(transform(c, position));
			}
		}
		System.out.printf("\nWord = %s (%d)", sb.toString(), sb.length());
		return sb.toString();
	}

	/**
	 * This method uses ENUMS to translate an INT value into a WORD.<br>
	 * 
	 * @param c : the character to translate
	 * @param position : the position (or column) in the String
	 * @return WORDS expressing a number
	 */
	private static String transform(char c, int position) {
		String value = Character.toString(c);
		String str = null;
		switch (position) {
		case 0:
			// Thousand column
			str = ZERO.equals(value) ? EMPTY : Thousand.getConvertibleString(Integer.parseInt(value));
			break;
		case 1:
			// Hundred column
			str = ZERO.equals(value) ? EMPTY : Hundred.getConvertibleString(Integer.parseInt(value));
			break;
		case 2:
			// Decade column
			str = ZERO.equals(value) ? EMPTY : Decade.getConvertibleString(Integer.parseInt(value));
			break;
		case 3:
			// Unit column
			str = ZERO.equals(value) ? EMPTY : Unit.getConvertibleString(Integer.parseInt(value));
		}
		return str;
	}

}
