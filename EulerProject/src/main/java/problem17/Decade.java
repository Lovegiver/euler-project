package problem17;

public enum Decade {

	TWENTY(2),
	THIRTY(3),
	FORTY(4),
	FIFTY(5),
	SIXTY(6),
	SEVENTY(7),
	EIGHTY(8),
	NINETY(9);
	
	private final static String EMPTY = "";
	
	public final int value;
	
	private Decade(int value) {
		this.value = value;
	}
	
	public int lengthOf(int val) {
		String convertedValue = getConvertibleString(val);
		return convertedValue.length();
	}
	
	public static String getConvertibleString(int val) {
		String str = null;
		switch (val) {
		case 0:
			str = EMPTY;
			break;
		case 2:
			str = Decade.TWENTY.name();
			break;
		case 3:
			str = Decade.THIRTY.name();
			break;
		case 4:
			str = Decade.FORTY.name();
			break;
		case 5:
			str = Decade.FIFTY.name();
			break;
		case 6:
			str = Decade.SIXTY.name();
			break;
		case 7:
			str = Decade.SEVENTY.name();
			break;
		case 8:
			str = Decade.EIGHTY.name();
			break;
		case 9:
			str = Decade.NINETY.name();
		}
		return str;
	}
	
}
