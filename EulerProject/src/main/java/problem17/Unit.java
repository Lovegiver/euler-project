package problem17;

/**
 * Units should be numbers from 1 to 9 inclusive.<br>
 * Nevertheless, values from 10 to 19 can't be expressed with the usual pattern
 * based on a DECADE number and a UNIT number like TWENTY-ONE or FORTY-TWO.<br>
 * Instead we used specific words like ELEVEN or TWELVE that behave just like 
 * unit numbers.<br>
 * This UNIT enum is extended to all values from 1 to 19 inclusive.<br>
 * <br>
 * @author Loveg
 *
 */
public enum Unit {

	ONE(1),
	TWO(2),
	THREE(3),
	FOUR(4),
	FIVE(5),
	SIX(6),
	SEVEN(7),
	EIGHT(8),
	NINE(9),
	TEN(10),
	ELEVEN(11),
	TWELVE(12),
	THIRTEEN(13),
	FOURTEEN(14),
	FIFTEEN(15),
	SIXTEEN(16),
	SEVENTEEN(17),
	EIGHTEEN(18),
	NINETEEN(19);
	
	private final static String EMPTY = "";
	
	public final int value;
	
	private Unit(int value) {
		this.value = value;
	}
	
	/**
	 * For a given INT value from 0 to 19, return the LENGTH of corresponding String.<br>
	 * Example : 1 -> ONE -> 3<br>
	 * Example : 7 -> SEVEN -> 5<br>
	 * 
	 * @param val : INT value to translate in words.
	 * @return : the length of the corresponding word.
	 */
	public static int lengthOf(int val) {
		String convertedValue = getConvertibleString(val);
		return convertedValue.length();
	}
	
	/**
	 * For a given INT value from 0 to 19, return the corresponding String.<br>
	 * Example : 1 -> ONE<br>
	 * Example : 7 -> SEVEN<br>
	 * 
	 * @param val : INT value to translate in words.
	 * @return : words expressing a number
	 */
	public static String getConvertibleString(int val) {
		String str = null;
		switch (val) {
		case 0:
			str = EMPTY;
			break;
		case 1:
			str = Unit.ONE.name();
			break;
		case 2:
			str = Unit.TWO.name();
			break;
		case 3:
			str = Unit.THREE.name();
			break;
		case 4:
			str = Unit.FOUR.name();
			break;	
		case 5:
			str = Unit.FIVE.name();
			break;	
		case 6:
			str = Unit.SIX.name();
			break;	
		case 7:
			str = Unit.SEVEN.name();
			break;	
		case 8:
			str = Unit.EIGHT.name();
			break;
		case 9:
			str = Unit.NINE.name();
			break;
		case 10:
			str = Unit.TEN.name();
			break;
		case 11:
			str = Unit.ELEVEN.name();
			break;
		case 12:
			str = Unit.TWELVE.name();
			break;
		case 13:
			str = Unit.THIRTEEN.name();
			break;
		case 14:
			str = Unit.FOURTEEN.name();
			break;	
		case 15:
			str = Unit.FIFTEEN.name();
			break;	
		case 16:
			str = Unit.SIXTEEN.name();
			break;	
		case 17:
			str = Unit.SEVENTEEN.name();
			break;	
		case 18:
			str = Unit.EIGHTEEN.name();
			break;
		case 19:
			str = Unit.NINETEEN.name();
		}
		return str;
	}
	
}
