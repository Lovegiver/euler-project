package problem17;

public enum Thousand {

	THOUSAND;
	
	private final static String EMPTY = "";
	
	public static int lengthOf(int val) {
		String convertedValue = getConvertibleString(val);
		return convertedValue.length();
	}
	
	public static String getConvertibleString(int val) {
		String str = null;
		switch (val) {
		case 0:
			str = EMPTY;
			break;
		default:
			str = Unit.getConvertibleString(val) + THOUSAND.name();
		}
		return str;
	}
	
}
