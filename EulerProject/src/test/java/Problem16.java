import static org.junit.Assert.assertTrue;

import java.math.BigInteger;

import org.junit.jupiter.api.Test;

class Problem16 {

	@Test
	void test() {
		long start = System.currentTimeMillis();
		String strResult = new BigInteger("2").pow(1000).toString();
		System.out.printf("Chaine trouv�e : %s.\nLongueur : %d caract�res.", strResult, strResult.length());
		assertTrue(strResult.length() == 302);
		long result = strResult.chars().reduce(0, (x, y) -> x + (Integer.parseInt(Character.toString(y))));
		long end = System.currentTimeMillis();
		System.out.printf("\nSomme des chiffres de Math.pow(2,1000) = %d. Trouv� en %d ms.", result, end - start);
	}

}
