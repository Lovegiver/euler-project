import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.LinkedList;
import java.util.function.Function;
import java.util.function.Supplier;

import org.junit.jupiter.api.Test;

class Problem14 {

	@SuppressWarnings("unchecked")
	@Test
	public void problem13Test() {

		Function<Long, Long> even = e -> e / 2;
		Function<Long, Long> odd = o -> 3 * o + 1;
		@SuppressWarnings("rawtypes")
		Function<Long, Function> choseFormula = value -> {
			return value % 2 == 0 ? even : odd;
		};
		Supplier<LinkedList<Long>> longSupplier = () -> new LinkedList<Long>();

		long start = System.currentTimeMillis();
		System.out.println("----- Start time = " + new Date() + " -----\n");

		int max = 0;
		int value = 0;
		LinkedList<Long> maxList = longSupplier.get();

		for (value = 5; value < 1000000; value++) {
			LinkedList<Long> allSteps = longSupplier.get();
			long result = value;
			try {
				while (result != 1) {
					allSteps.add(result);
					result = (long) choseFormula.apply(result).apply(result);
				}
			} catch (Exception e1) {
				System.out.println("ERROR FOR VALUE " + value);
				e1.printStackTrace();
			}
			allSteps.add(1L);
			assertTrue(allSteps.size() > 0);
			if (max < allSteps.size()) {
				max = allSteps.size();
				maxList.clear();
				maxList = allSteps;
			}
		}

		long end = System.currentTimeMillis();
		System.out.printf("Longest chain, found for [ %d ], counts %d elements. Solved in %d ms.\n\n", maxList.get(0),
				maxList.size(), end - start);
		System.out.println(maxList);
		System.out.println("\n----- End time = " + new Date() + " -----");
	}

}
