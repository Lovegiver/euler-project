import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigInteger;
import java.util.function.Function;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

class Problem20 {

	@Test
	void factorialTest() {

		BigInteger factorial100 = Stream.iterate(BigInteger.ONE, b -> b.add(BigInteger.ONE)).limit(99)
				.reduce(BigInteger.ONE, (x, y) -> x.multiply(y));

		assertTrue(factorial100.signum() == 1);

		System.out.println(factorial100);

		String factorialString = factorial100.toString();

		assertTrue(factorialString.length() > 0);

		Function<Integer, Integer> translateToInt = c -> {
			return Character.getNumericValue(c);
		};

		int sum = factorialString.chars().map(ch -> translateToInt.apply(ch)).reduce(0, (x, y) -> x + y);

		assertTrue(sum > 0);

		System.out.println("Somme = " + sum);

	}

}
