import org.junit.jupiter.api.Test;

class Problem9 {

	@Test
	public void problem9Test() {

		final int PERIMETRE = 1000;
		int minA = 1, minB = 2;
		int a = 0, b = 0, c = 0;
		boolean success = Boolean.FALSE;

		for (c = PERIMETRE - minB - minA; c > b; c--) {

			if (PERIMETRE - c - minA < c) {
				a = minA;
				b = PERIMETRE - c - a;
			} else {
				b = c - 1;
				a = PERIMETRE - c - b;
			}

			while (a < b && b < c && !success) {
				System.out.printf("\nA: %d, B: %d, C: %d -> A+B+C: %d\n", a, b, c, a + b + c);
				if (isPythOK(a, b, c, PERIMETRE)) {
					System.out.println("\nSUCCESS");
					System.out.printf("\nSolution is A=%d, B=%d, C=%d -> Product ABC = %d", a, b, c, a * b * c);
					success = Boolean.TRUE;
					break;
				}
				if(success) {
					break;
				} else {
					a++;
					b = PERIMETRE - c - a;
				}
			}
		}
	}

	private boolean isPythOK(int a, int b, int c, int perimetre) {
		long bigA = (long) Math.pow(a, 2);
		long bigB = (long) Math.pow(b, 2);
		long bigC = (long) Math.pow(c, 2);
		System.out.printf("A� + B� = %d + %d = %d", bigA, bigB, bigC);
		return (bigA + bigB == bigC);
	}

}
