import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class Problem2 {

	@Test
	void test() {

		List<Integer> fiboStart = new ArrayList<Integer>();
		fiboStart.add(1);
		fiboStart.add(2);
		int nextValue = 0;
		int fiboSum = 0;
		do {
			int arraySize = fiboStart.size();
			nextValue = fiboStart.get(arraySize - 1) + fiboStart.get(arraySize - 2);
			System.out.println("Value = " + nextValue);
			fiboStart.add(nextValue);
			fiboSum = fiboStart.stream().filter(i -> i % 2 == 0).reduce(0, (x, y) -> x + y);
		} while (fiboSum <= 4000000);
		System.out.println("Somme = " + fiboSum);
	}

}
