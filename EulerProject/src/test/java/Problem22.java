import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;

class Problem22 {

	@Test
	void namesScoresTest() {

		String fileName = "E:\\Datas\\Downloads\\names.txt";
		
		try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName))) {

			//br returns as stream and convert it into a List
			//List<String> list = br.lines().collect(Collectors.toList());
			String[] list = br.readLine().split(",");
			
			System.out.println("SIZE = " + list.length);
			
			for(String name : list) {
				System.out.println(name);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
