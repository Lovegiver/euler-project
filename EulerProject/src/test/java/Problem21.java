import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

class Problem21 {

	@Test
	void amicableNumbers() {

		/**
		 * For a given Integer value, return a Set of all its divisors
		 */
		Function<Integer, Set<Integer>> getDivisors = val -> {
			Set<Integer> divisors = new HashSet<Integer>();
			for (int i = 1; i <= val / 2; i++) {
				if (val % i == 0) {
					divisors.add(i);
				}
			}
			return divisors;
		};

		/**
		 * Sum all Integer values of a Set of Integer
		 */
		Function<Set<Integer>, Integer> getDivisorsSum = divs -> {
			List<Integer> list = new ArrayList<>(divs);
			return list.stream().reduce(0, (x, y) -> x + y);
		};

		/**
		 * Test if a given Integer value is 'Amicable' with the sum of all its divisors
		 * Add a Key/Value pair in the Map only if numbers are 'Amicable' AND if Keys
		 * and Values are not the same Number
		 */
		Predicate<Integer> isAmicable = val -> {
			int first = getDivisorsSum.apply(getDivisors.apply(val));
			int second = getDivisorsSum.apply(getDivisors.apply(first));
			return val == second && first != second;
		};

		/**
		 * Apply the Predicate in order to build a Map of Amicable Numbers
		 */
		Map<Integer, Integer> amicableNumbers = Stream.iterate(1, i -> ++i).limit(10000)
				.filter(value -> isAmicable.test(value)).collect(
						Collectors.toMap(Function.identity(), value -> getDivisorsSum.apply(getDivisors.apply(value))));

		System.out.println(amicableNumbers);

		/**
		 * Sum all the Keys of the Map (Values of the Map are identical to Keys)
		 */
		int result = amicableNumbers.entrySet().stream().map(e -> e.getKey()).reduce(0, (x, y) -> x + y);

		System.out.println("Result = " + result);

	}

}
