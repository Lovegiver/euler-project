import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import classes.Utilz;

class Problem10 {

	@Test
	public void problem10Test() {

		long start = System.currentTimeMillis();
		
		Stream<Long> stream = Utilz.getStream(2, 2000000, 1);
		List<Long> primesList = stream.filter(x -> Utilz.isPrime(x)).collect(Collectors.toList());
		long result = primesList.stream().reduce(0L, (x, y) -> x + y);
		
		long end = System.currentTimeMillis();
		System.out.printf("%d valeurs trouv�es ; somme = %d ; en %d ms.", primesList.size(), result, end - start);

	}

}
