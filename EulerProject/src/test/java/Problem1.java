import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

class Problem1 {

	@Test
	void test() {

		int intSum = Stream.iterate(1, i -> ++i)
				.limit(999)
				.filter(i -> i % 3 == 0 || i % 5 == 0)
				.reduce(0, (x, y) -> x + y);
		System.out.println(intSum);
	}

}
