import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

class Problem6 {

	@Test
	void test() {

		long squaredLong = Stream.iterate(1, l -> l + 1).limit(100L).reduce((int) 0L, (x, y) -> x + (y * y));
		long squaredSum = (long) Math.pow(Stream.iterate(1, l -> l + 1).limit(100L).reduce(0, (x, y) -> x + y), 2);
		System.out.printf("R�sultat : %d -> %d - %d", squaredSum - squaredLong, squaredSum, squaredLong);
		
	}

}
