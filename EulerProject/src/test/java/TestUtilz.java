import org.junit.jupiter.api.Test;

import classes.Counterpart;
import classes.Funder;
import classes.Supplier;
import classes.Utilz;

class TestUtilz {

	@Test
	void testIsPrime() {
		long value = 600851475143L;
		System.out.println(value + " is prime ? : " + Utilz.isPrime(value));
	}
	
	@Test
	void testCounterpart() {
		
		System.out.println("\nM�thodes abstraites de la classe m�re.\n");
		
		Counterpart supplier = new Supplier();
		supplier.setName("MySupplier");
		System.out.println("\n--> " + supplier.getClass().getSimpleName());
		
		Counterpart funder = new Funder();
		funder.setName("MyFunder");
		System.out.println("\n--> " + funder.getClass().getSimpleName());
		
		System.out.println(supplier.getName());
		System.out.println(funder.getName());
		
		System.out.println("\nM�thodes de la classe fille.\n");
		
		Supplier supplier2 = new Supplier();
		supplier2.printWord("\nSupplier strict utilisant m�thode de la classe m�re.\n");
		System.out.println("\n--> " + supplier2.getClass().getSimpleName());
		
		Funder funder2 = new Funder();
		System.out.println(funder2.concatWords("\nSeul un Funder strict peut utiliser une m�thode de la classe Funder.\n", 
				"Impossible d'utiliser cette m�thode depuis un objet Counterpart\n"));
		System.out.println("\n--> " + funder2.getClass().getSimpleName());
		
		Supplier supplier3 = (Supplier) Counterpart.getSupplier();
		Funder funder3 = (Funder) Counterpart.getFunder();
		Counterpart funder4 = Counterpart.getFunder();
		
		Counterpart funder5 = Counterpart.getFunderWithName("Fred");
		printName(funder5);
		System.out.println(Counterpart.funderWordsConcatenate("Fred", "Courcier"));
		
		Supplier supplier4 = Counterpart.getInstanceOfSupplier();
		Funder funder6 = Counterpart.getInstanceOfFunder();
		changeName(funder6,"Lola");
		
		printName(funder6);
		printName(funder5);
		
		changeName(funder5,"Fred");
		
		printName(funder6);
		printName(funder5);
		
	}
	
	private void printName(Counterpart counterpart) {
		System.out.println("Print name = " + counterpart.getName());
	}
	
	private void changeName(Counterpart counterpart, String name) {
		counterpart.setName(name);
		printName(counterpart);
	}

}
