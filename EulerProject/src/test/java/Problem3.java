import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class Problem3 {

	@Test
	void test() {

		long n = 600851475143L;
		List<Long> primeFactors = new ArrayList<Long>();
		long max = 0L;
		for (long i = 2; i <= n; i++) {
			while (n % i == 0L) {
				max = i;
				primeFactors.add(max);
				n /= i;
				System.out.println("Max = " + max);
			}
		}
		System.out.println(primeFactors);
		long verif = primeFactors.stream().reduce((long) 1, (x, y) -> x * y);
		System.out.println("Verification = " + verif);
		System.out.println(Long.compare(n, verif) == 0);
		System.out.println("Max factor = " + primeFactors.get(primeFactors.size() - 1));
	}

}
