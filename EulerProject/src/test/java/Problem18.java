import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import problem18.Composite;
import problem18.Coordinate;
import problem18.Grid;
import problem18.Node;

class Problem18 {

	/*
	 * Array of String containing all lines of the given grid
	 */
	private final String[] grid = {
			"75",
			"95 64",
			"17 47 82",
			"18 35 87 10",
			"20 04 82 47 65",
			"19 01 23 75 03 34",
			"88 02 77 73 07 63 67",
			"99 65 04 28 06 16 70 92",
			"41 41 26 56 83 40 80 70 33",
			"41 48 72 33 47 32 37 16 94 29",
			"53 71 44 65 25 43 91 52 97 51 14",
			"70 11 33 28 77 73 17 78 39 68 17 57",
			"91 71 52 38 17 14 91 43 58 50 27 29 48",
			"63 66 04 68 89 53 67 30 73 16 69 87 40 31",
			"04 62 98 27 23 09 70 98 73 93 38 53 60 04 23"};
	
	/**
	 * Builds a {@link Grid} object in which all values have been translated into
	 * {@link Node} objects.<br>
	 * For each value in the Grid, location within the Grid will be translated into
	 * a {@link Coordinate} object which is a Node's property.<br>
	 * 
	 * @return a {@link Grid} object composed of {@link Node} objects
	 */
	private Grid buildGrid() {
		List<Composite> cells = new ArrayList<Composite>();
		int y = 0;
		int x = 0;
		for (String line : grid) {
			String[] elements = line.split(" ");
			x = 0;
			for (String str : elements) {
				Node cell = Node.getNode(Integer.parseInt(str), x, y);
				x++;
				cells.add(cell);
			}
			y++;
		}
		Grid grid = new Grid(cells);
		return grid;
	}
	
	@Test
	void problem18() {
		long start = System.currentTimeMillis();

		// Build the Grid
		Grid grid = buildGrid();
		assertFalse(grid.getCells().isEmpty());
		
		// For each Node of the Grid, looks for children
		for (Composite c : grid.getCells()) {
			c.fillChilds();
			// System.out.println(c);
		}
		
		/*
		 * Starting from the Node at the top of the Grid (first element of the Grid)
		 * will analyze the Grid to find the path with the highest sum. Each path will
		 * contain 15 nodes : starting node + 14 child nodes. Number of analyzed nodes
		 * is hold by the 'depth' argument.
		 */
		Composite nodeToAnalyze = grid.getCells().get(0);
		int depth = 14;
		List<Composite> path = nodeToAnalyze.buildMaxPath(nodeToAnalyze, depth);

		long end = System.currentTimeMillis();
		
		for (Composite c : path) {
			System.out.println("Max Path = " + c.getCoordinate() + " | " + c.getValue());
		}
		System.out.printf("\nResult found in %d ms.", end - start);
	}

}
