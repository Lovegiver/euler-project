import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import problem17.Converter;
import problem17.Unit;

class Problem17 {

	//@Test
	void test() {
		long start = System.currentTimeMillis();
		Unit unit = Unit.EIGHT;
		int l = Unit.lengthOf(unit.value);
		assertEquals(5, l);
		long end = System.currentTimeMillis();
		System.out.printf("\nLongueur de %s = %d. Trouv� en %d ms.", unit.name(), l, end - start);
	}

	//@Test
	void converterTest() {
		String convertedValue = Converter.stringOf(23);
		assertEquals("0023", convertedValue);
		System.out.println(convertedValue + " from Converter");
	}

	//@Test
	void convertNumbersToText() {
		for (int i = 1; i < 20; i++) {
			String str = null;
			str = Converter.wordsOf(Converter.stringOf(i));
			System.out.println("\nStringBuilder = " + str);
		}
	}
	
	//@Test
	void getLengthOfWords() {
		for (int i = 1; i < 25; i++) {
			System.out.println("\nLength = " + Converter.lengthOf(i));
		}
	}

	@Test
	void problem17() {
		long start = System.currentTimeMillis();
		int resultat = Stream.iterate(1, i -> i + 1).limit(1000).reduce(0, (x, y) -> x + Converter.lengthOf(y));
		long end = System.currentTimeMillis();
		System.out.printf("\nNombre cumul� de caract�res = %d. Trouv� en %d ms.", resultat, end - start);

	}

}
