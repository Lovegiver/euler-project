import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

class Problem5 {

	@Test
	void test() {
		System.out.println("\nStarting Test : plain old Java\n");
		long start = System.currentTimeMillis();
		boolean foundValue = false;
		long valueToFindOut = 20L;
		List<Long> divisors = Arrays.asList(2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L, 11L, 12L, 13L, 14L, 15L, 16L, 17L, 18L,
				19L, 20L);

		while (!foundValue) {
			boolean found = false;
			for (long div : divisors) {
				if (isDivisible(valueToFindOut, div)) {
					found = true;
				} else {
					found = false;
					break;
				}
			}
			if (!found) {
				valueToFindOut += 20L;
			} else {
				foundValue = true;
				System.out.println("Valeur trouv�e = " + valueToFindOut);
			}
		}
		long end = System.currentTimeMillis();
		System.out.println("R�sultat obtenu en " + (end - start) + " millisecondes");
	}

	@Test
	void testLambda() {
		System.out.println("\nStarting Test : functional Java\n");
		long start = System.currentTimeMillis();
		List<Long> divisors = Arrays.asList(2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L, 11L, 12L, 13L, 14L, 15L, 16L, 17L, 18L,
				19L, 20L);
		Predicate<Long> predicate = longPredicate(divisors);
		long result = generateLongStream().filter(predicate).findFirst().get();
		long end = System.currentTimeMillis();
		System.out.println("Resultat = " + result + " obtenu en " + (end - start) + " millisecondes.");
	}

	private boolean isDivisible(long toDivide, long divisor) {
		//System.out.printf("\n%d / %d",toDivide,divisor);
		return toDivide % divisor == 0;
	}

	private Stream<Long> generateLongStream() {
		return Stream.iterate(20L, l -> l + 20L).parallel();
	}
	
	private Predicate<Long> longPredicate(List<Long> longs) {
		long start = System.currentTimeMillis();
		Predicate<Long> predicate = null;
		if(!(longs.isEmpty())) {
			List<Predicate<Long>> predicates = new ArrayList<Predicate<Long>>(longs.size());
			longs.forEach(divisor -> {
				predicates.add(valueToTest -> isDivisible(valueToTest, divisor));
			});
			for(int i = predicates.size() -1 ; i >= 0 ; i--) {
				if(i == predicates.size() -1) {
					predicate = predicates.get(i);
				} else {
					predicate = predicate.and(predicates.get(i));
				}
			}
		}
		long end = System.currentTimeMillis();
		System.out.println("Predicate construit en " + (end - start) + " millisecondes.");
		return predicate;
	}

}
