import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.jupiter.api.Test;

class Problem4 {

	@Test
	void test() {
		long start = System.currentTimeMillis(); 
		long max = 0;
		Map<Integer,Integer> cache = new HashMap<Integer,Integer>();
		for (int i = 999; i > 0; i--) {
			for (int j = 999; j > 0; j--) {
				if(!(cache.containsKey(j) && cache.containsValue(i))) {
					cache.put(i, j);
					int produit = i * j;
					String produitString = Integer.toString(produit);
					if(produitString.length() > 1) {
						// Nombre pair de chiffres
						if (produitString.length() % 2 == 0) {
							Pair<String, String> pair = Pair.of(produitString.substring(0, produitString.length() / 2),
									produitString.substring(produitString.length() / 2, produitString.length()));
							String reversed = reverseWord(pair.getRight());
							if (pair.getLeft().equals(reversed)) {
								max = Math.max(max, produit);
								System.out.printf("\nChaine trouv�e : %s, r�sultat de %d x %d", produitString, i, j);
							}
						} else {
							// Nombre impair de chiffres
							Triple<String, String, String> triple = Triple.of(
									produitString.substring(0, produitString.length() / 2),
									produitString.substring(produitString.length() / 2, produitString.length() / 2 + 1),
									produitString.substring(produitString.length() / 2 + 1, produitString.length()));
							String reversed = reverseWord(triple.getRight());
							if (triple.getLeft().equals(reversed)) {
								max = Math.max(max, produit);
								System.out.printf("\nChaine trouv�e : %s, r�sultat de %d x %d", produitString, i, j);
							}
						}
					}
				}
			}
		}
		long end = System.currentTimeMillis();
		System.out.println("\n\nMAX = " + max + " trouv� en " + ((end - start)) + " millisecondes");
	}

	private String reverseWord(final String word) {
		String reversedWord = "";
		Stack<String> stack = new Stack<String>();

		for (Character c : word.toCharArray()) {
			String s = String.valueOf(c);
			stack.add(s);
		}
		while (!stack.empty()) {
			reversedWord += stack.pop();
		}

		return reversedWord;
	}

}
