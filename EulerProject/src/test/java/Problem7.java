import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import classes.Utilz;

class Problem7 {

	@Test
	void test10001() {

		long start = System.currentTimeMillis();
		List<Long> primes = new ArrayList<Long>();
		primes.add((long) 2);
		long init = 3L;
		final int objective = 10001;

		while (primes.size() < objective) {
			if (Utilz.isPrime(init)) {
				primes.add(init);
			}
			init++;
		}
		long end = System.currentTimeMillis();
		assertEquals(objective, primes.size());
		System.out.printf("%d prime numbers found in %d ms", primes.size(), end - start);
	}

}
