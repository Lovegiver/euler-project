import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.jupiter.api.Test;

class Problem12 {
	
	@Test
	public void problem12v2Test() {

		long start = System.currentTimeMillis();
		long result = 0;
		long step = 1L;
		List<Long> divisors = new ArrayList<Long>();

		System.out.println("Start time = " + new Date());
		while (!(divisors.size() > 500)) {
			result += step++;
			//System.out.println("Valeur de Result = " + result);
			divisors.clear();
			final long r = result;
			for (long div = 1L; div <= r; div++) {
				if (r % div == 0) {
					divisors.add(div);
				}
			}
			//System.out.println(divisors);
		}
		long end = System.currentTimeMillis();
		System.out.printf("La valeur %d contient %d diviseurs. R�sultat obtenu en %d ms.", result, divisors.size(),
				end - start);
		System.out.println("Liste des valeurs : " + divisors);
		System.out.println("End time = " + new Date());
	}

}
